import java.math.BigDecimal;

public class SimpleVatProvider implements VatProvider{


    @Override
    public BigDecimal getDefaultVat() {
        return new BigDecimal ("0.23");
    }

    @Override
    public BigDecimal getVatForType(String type) {
        return null;
    }
}
