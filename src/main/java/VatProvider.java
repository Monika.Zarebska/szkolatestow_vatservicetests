import java.math.BigDecimal;

public interface VatProvider {

    public abstract BigDecimal getDefaultVat();

    public abstract BigDecimal getVatForType(String type);
}
