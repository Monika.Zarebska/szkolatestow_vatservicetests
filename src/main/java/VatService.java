import java.math.BigDecimal;
import java.math.RoundingMode;

public class VatService {

    private VatProvider vatProvider;
    private Logger logger;

    public VatService(VatProvider vatProvider, Logger logger) {
        this.vatProvider = vatProvider;
        this.logger = logger;
    }

    public BigDecimal getGrossPriceForDefaultVat(Product product) throws Exception {
        return getGrossPrice(product.getNetPrice(), vatProvider.getDefaultVat());
    }

    public BigDecimal getGrossPriceForProduct(Product product) throws Exception {
        return getGrossPrice(product.getNetPrice(), vatProvider.getVatForType(product.getType()));
    }

    private BigDecimal getGrossPrice(BigDecimal netPrice, BigDecimal vatValue) throws Exception {
        logger.log("Info", "Starting calculating price.");
        errorOnIncorrectVat(vatValue);
        logger.log("Info","Price calculated successfully.");
        return netPrice.multiply(BigDecimal.ONE.add(vatValue)).setScale(2, RoundingMode.HALF_EVEN);
    }

    private void errorOnIncorrectVat(BigDecimal vatValue) throws Exception {
        if (vatValue.compareTo(BigDecimal.ONE) > 0) {
            logger.log("Error","Vat cannot be greater than 1.");
            throw new Exception("Vat bigger than 1");
        }
        if (vatValue.compareTo(BigDecimal.ZERO) < 0) {
            logger.log("Error","Vat cannot be lower than 0.");
            throw new Exception("Vat lower than 0.");
        }
    }
}
