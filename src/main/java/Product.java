import java.math.BigDecimal;

public class Product {

    private String id;

    private BigDecimal netPrice;

    private String type;

    public Product(String id, BigDecimal netPrice, String type) {
        this.id = id;
        this.netPrice = netPrice;
        this.type = type;
    }

    public BigDecimal getNetPrice() {
        return netPrice;
    }

    public String getType() {
        return type;
    }
}
