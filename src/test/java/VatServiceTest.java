import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;


class VatServiceTest {

    private static final Product PRODUCT_WITH_PRICE_5_05
            = new Product("someProductId", new BigDecimal("5.05"), "Some type");

    private VatProvider vatProviderMock;
    private Logger loggerMock;
    private VatService vatService;


    @BeforeEach
    void prepareVatService() {
        vatProviderMock = mock(VatProvider.class);  //udajemy VatProvidera, np. zewnetrzny serwis.
        loggerMock = mock(Logger.class);
        vatService = new VatService(vatProviderMock, loggerMock);
    }

    @Test
    void shouldReturnGrossValueForDefaultVat() throws Exception {

        when(vatProviderMock.getDefaultVat()).thenReturn(new BigDecimal("0.23"));

        BigDecimal result = vatService.getGrossPriceForDefaultVat(PRODUCT_WITH_PRICE_5_05);

        assertThat(result)
                .isEqualTo(new BigDecimal("6.21"));
    }

    @ParameterizedTest()
    @CsvSource({
            "0,5.05",
            "0.01,5.10",
            "1,10.10"
    })
    void shouldReturnGrossValueForNonDefaultVat(String vat, String grossPrice) throws Exception {

        when(vatProviderMock.getVatForType(PRODUCT_WITH_PRICE_5_05.getType()))
                .thenReturn(new BigDecimal(vat));

        BigDecimal result = vatService.getGrossPriceForProduct(PRODUCT_WITH_PRICE_5_05);

        assertThat(result)
                .isEqualTo(new BigDecimal(grossPrice));
    }

    @ParameterizedTest()
    @CsvSource({
            "-0.01,Vat lower than 0.",
            "1.01,Vat bigger than 1 "
    })
    void shouldThrowExceptionWhenVatIsNegativeOrTooHigh(String vat, String message) {
        when(vatProviderMock.getVatForType(PRODUCT_WITH_PRICE_5_05.getType()))
                .thenReturn(new BigDecimal(vat));

        assertThatThrownBy(() -> vatService.getGrossPriceForProduct(PRODUCT_WITH_PRICE_5_05))
                .hasMessage(message);
    }

    @Test
    void shouldLogWhenVatIsNegative() {
        when(vatProviderMock.getVatForType(PRODUCT_WITH_PRICE_5_05.getType()))
                .thenReturn(new BigDecimal("-0.01"));

        try {
            vatService.getGrossPriceForProduct(PRODUCT_WITH_PRICE_5_05);
        } catch (Exception ignore) {

        }
        verify(loggerMock).log("Info", "Starting calculating price.");
        verify(loggerMock).log("Error", "Vat cannot be lower than 0.");
    }

    @Test
    void shouldLogWhenVatIsTooHigh() {
        when(vatProviderMock.getVatForType(PRODUCT_WITH_PRICE_5_05.getType()))
                .thenReturn(new BigDecimal("1.01"));

        try {
            vatService.getGrossPriceForProduct(PRODUCT_WITH_PRICE_5_05);
        } catch (Exception ignore) {

        }

        verify(loggerMock).log("Info", "Starting calculating price.");
        verify(loggerMock).log("Error", "Vat cannot be greater than 1.");
    }

    @ParameterizedTest()
    @ValueSource(strings = {
            "0",
            "0.01",
            "1"
    })
    void shouldNotLogErrorForCorrectVat(String vat) throws Exception {
        when(vatProviderMock.getVatForType(PRODUCT_WITH_PRICE_5_05.getType()))
                .thenReturn(new BigDecimal(vat));

        vatService.getGrossPriceForProduct(PRODUCT_WITH_PRICE_5_05);

        verify(loggerMock).log("Info", "Starting calculating price.");
        verify(loggerMock, never()).log("Error", "Vat cannot be greater than 1.");
        verify(loggerMock, never()).log("Error", "Vat cannot be lover than 0.");
    }
}